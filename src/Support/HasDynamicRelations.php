<?php
namespace Spacework\Core\Support\Traits;

trait HasDynamicRelations
{
	private static $dynamicRelations = [];

	public static function defineDynamicRelation($name, $callback)
	{
        if (is_string($name) && is_callable($callback)) {
            static::$dynamicRelations[$name] = $callback;
        }
	}

	public static function hasDynamicRelation($name)
	{
		return array_key_exists($name, static::$dynamicRelations);
	}

	public function __get($name)
    {
        if (static::hasDynamicRelation($name)) {
            if ($this->relationLoaded($name)) {
                return $this->relations[$name];
			}
			
            return $this->getRelationshipFromMethod($name);
        }
        return parent::__get($name);
    }

	public function __call($name, $arguments)
    {
        if(static::hasDynamicRelation($name)) {
            return call_user_func(static::$dynamicRelations[$name], $this);
		}
		
        return parent::__call($name, $arguments);
    }
}