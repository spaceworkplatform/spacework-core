<?php
use Spacework\Core\Facades\Core;

Route::get('version', function () {
	return Core::version();
});