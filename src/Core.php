<?php

namespace Spacework\Core;

class Core
{
	const VERSION = 1.0;
	
	public function version()
	{
		return static::VERSION;
	}
}